import { Body, Controller, Delete, Get, Param, Put } from '@nestjs/common';
import { GlobalListItem } from '../types/dto/global-list-item.dto';
import { TodoListItem } from '../types/dto/todo-list-item.dto';
import { CreateListRequest } from '../types/dto/create-list.dto';
import { TodoService } from '../services/todo.service';

@Controller('todo/list')
export class TodoListController {
  @Get('global')
  getGlobalList(): GlobalListItem[] {
    return [
      {
        id: 1,
        name: 'One',
      },
      {
        id: 2,
        name: 'Two',
      },
    ];
  }

  @Get(':id')
  getTodoList(@Param('id') todoListId: number): TodoListItem[] {
    return [
      {
        id: 1,
        content: 'Eat',
        checked: true,
      },
      {
        id: 2,
        content: 'Drink',
        checked: false,
      },
    ];
  }

  @Put(':id?')
  async putTodoList(
    @Param('id') todoListId: number | undefined,
    @Body() requestData: CreateListRequest,
  ): Promise<number> {
    if (!todoListId) {
      return await this.todoService.createTodoList(requestData.name);
    }

    await this.todoService.editTodoList(todoListId, requestData.name);
    return Promise.resolve(todoListId);
  }

  @Delete(':id')
  deleteTodoList(@Param('id') todoListId: number): boolean {
    return true;
  }

  constructor(private readonly todoService: TodoService) {}
}
