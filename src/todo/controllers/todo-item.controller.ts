import { Body, Controller, Delete, Get, Param, Put } from '@nestjs/common';
import { CreateTodoItemRequest } from '../types/dto/create-todo-item.dto';

@Controller('todo/item')
export class TodoItemController {
  @Get(':id/check')
  checkItem(@Param('id') itemId: number | undefined): boolean {
    return true;
  }

  @Put(':id?')
  putTodoItem(
    @Param('id') itemId: number | undefined,
    @Body() requestData: CreateTodoItemRequest,
  ): boolean {
    return true;
  }

  @Delete(':id')
  deleteTodoItem(@Param('id') itemId: number | undefined): boolean {
    return true;
  }
}
