import { Injectable } from '@nestjs/common';
import { TodoListRepo } from '../repositories/todo-list-repo.service';

@Injectable()
export class TodoService {
  constructor(private readonly todoListRepo: TodoListRepo) {}

  async createTodoList(name: string): Promise<number> {
    return await this.todoListRepo
      .createTodoList(name)
      .then((todoList) => todoList.id);
  }

  async editTodoList(id: number, name: string): Promise<number> {
    return await this.todoListRepo.updateTodoList(id, name).then(() => id);
  }
}
