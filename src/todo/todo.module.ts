import { Module } from '@nestjs/common';
import { TodoListController } from './controllers/todo-list.controller';
import { TodoItemController } from './controllers/todo-item.controller';
import { TodoService } from './services/todo.service';
import { DatabaseModule } from '../database/database.module';
import { TodoListRepo, todoRepoProvider } from './repositories/todo-list-repo.service';

@Module({
  imports: [DatabaseModule],
  controllers: [TodoListController, TodoItemController],
  providers: [TodoService, TodoListRepo, ...todoRepoProvider],
})
export class TodoModule {}
