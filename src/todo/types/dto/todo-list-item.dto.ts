export class TodoListItem {
  id: number;
  content: string;
  checked: boolean;
}
