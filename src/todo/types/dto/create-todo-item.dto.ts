export class CreateTodoItemRequest {
  name: string;
  todoListId: number;
}
