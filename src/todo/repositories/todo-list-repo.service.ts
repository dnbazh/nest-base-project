import { Inject, Injectable, Provider } from '@nestjs/common';
import { RepositoryEnum } from '../../database/repository.enum';
import { Connection, Repository, UpdateResult } from 'typeorm';
import { TodoListEntity } from '../entity/todo-list.entity';

@Injectable()
export class TodoListRepo {
  constructor(
    @Inject(RepositoryEnum.TODO_LIST_REPOSITORY)
    private readonly todoListRepo: Repository<TodoListEntity>,
  ) {}

  createTodoList(name: string): Promise<TodoListEntity> {
    return this.todoListRepo.save(new TodoListEntity({ name }));
  }

  updateTodoList(id: number, name: string): Promise<UpdateResult> {
    return this.todoListRepo.update(
      {
        id,
      },
      {
        name,
      },
    );
  }
}

export const todoRepoProvider: Provider = {
  provide: RepositoryEnum.TODO_LIST_REPOSITORY,
  useFactory: (connection: Connection) =>
    connection.getRepository(TodoListEntity),
  inject: ['DATABASE_CONNECTION'],
};
