import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { TodoListEntity } from './todo-list.entity';

@Entity()
export class TodoItemEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({
    default: false,
  })
  checked: boolean;

  @ManyToOne(() => TodoListEntity, { onDelete: 'CASCADE' })
  todoList: TodoListEntity;

  @UpdateDateColumn()
  updated_at: Date;

  @CreateDateColumn()
  created_at: Date;
}
