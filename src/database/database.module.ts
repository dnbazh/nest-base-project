import { Module } from '@nestjs/common';
import { createConnection } from 'typeorm';
import { TodoListEntity } from '../todo/entity/todo-list.entity';
import { TodoItemEntity } from '../todo/entity/todo-item.entity';

export const databaseProviders = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: async () =>
      await createConnection({
        type: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: 'root',
        database: 'nest-todo-test',
        entities: [TodoListEntity, TodoItemEntity],
        synchronize: false,
      }),
  },
];

@Module({ providers: [...databaseProviders], exports: [...databaseProviders] })
export class DatabaseModule {}
